;; Disable package enable at startup
(setq package-enable-at-startup nil)
(setq inhibit-splash-screen t)
(setq load-prefer-newer t)

;; Faster to disable these here (before they've been initialized)
;; (push '(menu-bar-lines . 0) default-frame-alist)
(push '(menu-bar-lines . 0) default-frame-alist)
(push '(tool-bar-lines . 0) default-frame-alist)
(push '(vertical-scroll-bars) default-frame-alist)
(push '(alpha-background . 80) default-frame-alist)

;; (when (featurep 'cairo)
;;   (push '(internal-border-width . 12) default-frame-alist))

;; NS gui settings
(when (featurep 'ns)
  (push '(ns-transparent-titlebar . t) default-frame-alist)
  (add-to-list 'default-frame-alist '(undecorated-round . t)))
  ;; (push '(alpha . 90) default-frame-alist))

;; Defer garbage collection further back in the startup process
(setq gc-cons-threshold most-positive-fixnum
      gc-cons-percentage 0.5)

;; Set default font before frame creation to make sure the first frame have the correct size
(defvar font-list
  (cond
   ((eq system-type 'darwin)
    '(("MonoLisa Nasy" . 15) ("Monaco" . 13) ("Menlo" . 13)))
   ((eq system-type 'windows-nt)
    '(("mononoki NF" . 12) ("Consolas" . 12) ("Cascadia Mono" . 11)))
   (t
    '(("MonoLisa Nasy" . 11) ("SF Mono" . 11) ("Consolas" . 12))))
  "List of fonts and sizes.  The first one available will be used.")

(add-to-list 'default-frame-alist (cons 'font (format "%s-%d" (caar font-list) (cdar font-list))))


;; Add all submodule theme to load-path
;; (let ((basedir "~/.emacs.d/themes/"))
;;   (dolist (f (directory-files basedir))
;;     (if (and (not (or (equal f ".") (equal f "..")))
;;              (file-directory-p (concat basedir f)))
;;         (add-to-list 'custom-theme-load-path (concat basedir f)))))

;; Only add nasy-theme
;; (add-to-list 'custom-theme-load-path "~/.emacs.d/themes/nasy-theme/")
