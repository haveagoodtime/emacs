{
  description = "Emacs Develop Shell";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs";
    flake-utils.url = "github:numtide/flake-utils";
    emacs-overlay.url = "github:nix-community/emacs-overlay";
  };

  outputs = inputs:
    inputs.flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import inputs.nixpkgs {
          inherit system;
          overlays = [ inputs.emacs-overlay.overlay ];
        };
      in rec {
        # `nix develop`
        devShell = pkgs.mkShell rec {
          nativeBuildInputs = with pkgs; [ gnumake emacsPgtk nixfmt ];
        };
      });

}
