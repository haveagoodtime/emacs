#+setupfile: https://fniessen.github.io/org-html-themes/org/theme-readtheorg.setup
#+html_head: <link rel="stylesheet" type="text/css" href="style.css">
#+PROPERTY: header-args:emacs-lisp :tangle (concat temporary-file-directory "init.el") :lexical t
#+export_file_name: ~/Repos/haveagoodtime.gitlab.io/EMACS.html

#+author: me
#+date: <2022-07-27 Wed>
#+title: Emacs 配置（2022 重置版） STABLE

* 🚩 Head                                                          :noexport:
  #+begin_src emacs-lisp :exports none
      ;;; init.el --- My Emacs Configuration file.            -*- lexical-binding: t; -*-
  #+end_src
* 📦 Straight/Package.el                                           :包管理器:
Straight Maybe Slow... Now I Switch to Package.el.

Maybe Modify ~straight-check-for-modifications~  Let Straight Fast.

Wow, Straight.el Rocks!
#+begin_src emacs-lisp
  ;;Straight ------------------------------------------------
  ;; Config

  (setq straight-vc-git-default-clone-depth 1)
  ;; (setq straight-vc-git-default-protocol 'ssh)
  (setq straight-check-for-modifications nil)

  ;; Bootstrap
  (defvar bootstrap-version)
  (let ((bootstrap-file
         (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
        (bootstrap-version 5))
    (unless (file-exists-p bootstrap-file)
      (with-current-buffer
          (url-retrieve-synchronously
           "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
           'silent 'inhibit-cookies)
        (goto-char (point-max))
        (eval-print-last-sexp)))
    (load bootstrap-file nil 'nomessage))


  ;; Use the built-in alternative
  (dolist (pkg '(eldoc flymake jsonrpc org project xref eglot))
    (add-to-list 'straight-built-in-pseudo-packages pkg))

  ;; package.el ------------------------------------------------

  ;; (package-initialize)
  ;; (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)

  ;; common alias ------------------------------------------------
  (defalias 'sup 'straight-use-package)
#+end_src
* 📔 Editor                                                  :编辑器基础设定:
  #+begin_src emacs-lisp
    ;; basic ------------------------------------------------
    ;; 自动加载外部修改过的文件
    (global-auto-revert-mode 1)
    ;; 关闭自己生产的保存文件
    (setq auto-save-default nil)
    ;; 关闭自己生产的备份文件
    (setq make-backup-files nil)
    ;; without lock files
    (setq create-lockfiles nil)
    ;; y-or-n
    (fset 'yes-or-no-p 'y-or-n-p)

    ;; macos ------------------------------------------------
    ;; macOS change meta key to command
    (when (featurep 'ns)
      (setq mac-command-modifier 'meta)
      (setq mac-option-modifier 'none))

    ;; basic mode ------------------------------------------------
    ;;auto pair
    (setq electric-pair-inhibit-predicate 'electric-pair-conservative-inhibit)
    (electric-pair-mode 1)
    ;; recentf
    (recentf-mode 1)
    ;; Save
    (savehist-mode 1)
    ;; 选中某个区域继续编辑可以替换掉该区域
    (delete-selection-mode 1)
  #+end_src
* 🏞️ GUI                                                        :基础界面设定:
  #+begin_src emacs-lisp
    ;; display “lambda” as “λ”
    (global-prettify-symbols-mode 1)

    ;; 像素滚动
    (pixel-scroll-precision-mode 1)
    (when (featurep 'cario)
      (setq pixel-scroll-precision-large-scroll-height 10
            pixel-scroll-precision-interpolation-factor 10.0))

    ;; hl-line
    (global-hl-line-mode 1)

    ;;行号
    ;; (when (fboundp 'display-line-numbers-mode)
    ;;   (add-hook 'prog-mode-hook 'display-line-numbers-mode))

    ;; mac ------------------------------------------------

    ;; Fix macos window border
    ;; https://lists.gnu.org/archive/html/bug-gnu-emacs/2018-09/msg00898.html
    (when (featurep 'ns)
      (set-frame-parameter nil 'internal-border-width 5))
  #+end_src
** 🌈 彩虹括号
#+begin_src emacs-lisp
  (sup 'rainbow-delimiters)
  (add-hook 'prog-mode-hook 'rainbow-delimiters-mode)
  (custom-set-faces
   '(rainbow-delimiters-base-face    ((t (:inherit nil :weight bold))))
   '(rainbow-delimiters-depth-1-face ((t (:inherit rainbow-delimiters-base-face :foreground "dark orange"))))
   '(rainbow-delimiters-depth-2-face ((t (:inherit rainbow-delimiters-base-face :foreground "DeepSkyBlue"))))
   '(rainbow-delimiters-depth-3-face ((t (:inherit rainbow-delimiters-base-face :foreground "YellowGreen"))))
   '(rainbow-delimiters-depth-4-face ((t (:inherit rainbow-delimiters-base-face :foreground "LimeGreen"))))
   '(rainbow-delimiters-depth-5-face ((t (:inherit rainbow-delimiters-base-face :foreground "dodger blue"))))
   '(rainbow-delimiters-depth-6-face ((t (:inherit rainbow-delimiters-base-face :foreground "MediumSlateBlue"))))
   '(rainbow-delimiters-depth-7-face ((t (:inherit rainbow-delimiters-base-face :foreground "blue violet"))))
   '(rainbow-delimiters-depth-8-face ((t (:inherit rainbow-delimiters-base-face :foreground "hot pink"))))
   '(rainbow-delimiters-depth-9-face ((t (:inherit rainbow-delimiters-base-face :foreground "DeepPink")))))
#+end_src
* ✨ Font                                                    :表情和中文字体:
  #+begin_src emacs-lisp
    ;; Emoji ------------------------------------------------

    ;; (set-fontset-font
    ;;  t
    ;;  '(#x1f300 . #x1fad0)
    ;;  (cond
    ;;   ((member "Noto Color Emoji" (font-family-list)) "Noto Color Emoji")
    ;;   ((member "Apple Color Emoji" (font-family-list)) "Apple Color Emoji")
    ;;   ((member "Noto Emoji" (font-family-list)) "Noto Emoji")
    ;;   ((member "Segoe UI Emoji" (font-family-list)) "Segoe UI Emoji")
    ;;   ((member "Symbola" (font-family-list)) "Symbola"))
    ;;  )

    ;; 中文 ------------------------------------------------

    ;; (defvar +font-unicode-family "Unifont")
    ;; (defun +load-ext-font ()
    ;;   (interactive)
    ;;   (when window-system
    ;;     (let ((font (frame-parameter nil 'font))
    ;;           (font-spec (font-spec :family +font-unicode-family)))
    ;;       (dolist (charset '(kana han hangul cjk-misc bopomofo symbol))
    ;;         (set-fontset-font font charset font-spec)))))
    ;; (+load-ext-font)
  #+end_src

* 👁️ Theme                                                          :主题设定:
** Theme Settings
#+begin_src emacs-lisp
    ;; Custom Theme ------------------------------------------------

    (custom-theme-set-faces
     'user
     '(fixed-pitch((t (:family "Monolisa Nasy" :height 1.0))))
     '(variable-pitch ((t (:family "Bookerly" :height 1.0))))
     '(mode-line ((t (:inherit variable-pitch :height 1.0))))
     '(mode-line-inactive ((t (:inherit variable-pitch :height 1.0))))
     '(Info-quoted ((t (:inherit variable-pitch))))
     '(org-block ((t (:inherit variable-pitch))))
     '(org-code ((t (:inherit (shadow fixed-pitch)))))
     '(org-document-info-keyword ((t (:inherit (shadow fixed-pitch)))))
     '(org-indent ((t (:inherit (org-hide fixed-pitch)))))
     '(org-meta-line ((t (:inherit (font-lock-comment-face fixed-pitch)))))
     '(org-property-value ((t (:inherit fixed-pitch))) t)
     '(org-special-keyword ((t (:inherit (font-lock-comment-face fixed-pitch)))))
     '(org-table ((t (:inherit fixed-pitch))))
     '(org-tag ((t (:inherit (shadow fixed-pitch) :weight bold :height 1.0))))
     '(org-verbatim ((t (:inherit (shadow fixed-pitch)))))
     )

    (add-hook 'org-mode-hook #'variable-pitch-mode)
    (add-hook 'Info-mode-hook #'variable-pitch-mode)

    ;; NS AUTO theme ------------------------------------------------
    ;; (sup 'spacemacs-theme)
    (sup 'ef-themes)

    (when (display-graphic-p)
      (when (featurep 'ns)
        (defun my/apply-theme (appearance)
          "Load theme, taking current system APPEARANCE into
            consideration."
          (mapc #'disable-theme custom-enabled-themes)
          (pcase appearance
            ('light (load-theme 'ef-summer t) (setq default-frame-alist
                                                         '((ns-appearance . light))))
            ('dark (load-theme 'ef-night t) (setq default-frame-alist
                                                       '((ns-appearance . dark))))))
        (add-hook 'ns-system-appearance-change-functions #'my/apply-theme)))
   (unless (featurep 'ns)
     (load-theme 'ef-elea-dark t))

#+end_src
** Custum Modus-Light
[[https://protesilaos.com/codelog/2022-07-26-modus-themes-color-override-demo/][Emacs: the Modus themes “Summertime” re-spin]]
#+begin_src emacs-lisp
  (setq modus-themes-operandi-color-overrides
        '((bg-main . "#fff0f2")
          (bg-dim . "#fbe6ef")
          (bg-alt . "#f5dae6")
          (bg-hl-line . "#fad8e3")
          (bg-active . "#efcadf")
          (bg-inactive . "#f3ddef")
          (bg-active-accent . "#ffbbef")
          (bg-region . "#dfc5d1")
          (bg-region-accent . "#efbfef")
          (bg-region-accent-subtle . "#ffd6ef")
          (bg-header . "#edd3e0")
          (bg-tab-active . "#ffeff2")
          (bg-tab-inactive . "#f8d3ef")
          (bg-tab-inactive-accent . "#ffd9f5")
          (bg-tab-inactive-alt . "#e5c0d5")
          (bg-tab-inactive-alt-accent . "#f3cce0")
          (fg-main . "#543f78")
          (fg-dim . "#5f476f")
          (fg-alt . "#7f6f99")
          (fg-unfocused . "#8f6f9f")
          (fg-active . "#563068")
          (fg-inactive . "#8a5698")
          (fg-docstring . "#5f5fa7")
          (fg-comment-yellow . "#a9534f")
          (fg-escape-char-construct . "#8b207f")
          (fg-escape-char-backslash . "#a06d00")
          (bg-special-cold . "#d3e0f4")
          (bg-special-faint-cold . "#e0efff")
          (bg-special-mild . "#c4ede0")
          (bg-special-faint-mild . "#e0f0ea")
          (bg-special-warm . "#efd0c4")
          (bg-special-faint-warm . "#ffe4da")
          (bg-special-calm . "#f0d3ea")
          (bg-special-faint-calm . "#fadff9")
          (fg-special-cold . "#405fb8")
          (fg-special-mild . "#407f74")
          (fg-special-warm . "#9d6f4f")
          (fg-special-calm . "#af509f")
          (bg-completion . "#ffc5e5")
          (bg-completion-subtle . "#f7cfef")
          (red . "#ed2f44")
          (red-alt . "#e0403d")
          (red-alt-other . "#e04059")
          (red-faint . "#ed4f44")
          (red-alt-faint . "#e0603d")
          (red-alt-other-faint . "#e06059")
          (green . "#217a3c")
          (green-alt . "#417a1c")
          (green-alt-other . "#006f3c")
          (green-faint . "#318a4c")
          (green-alt-faint . "#518a2c")
          (green-alt-other-faint . "#20885c")
          (yellow . "#b06202")
          (yellow-alt . "#a95642")
          (yellow-alt-other . "#a06f42")
          (yellow-faint . "#b07232")
          (yellow-alt-faint . "#a96642")
          (yellow-alt-other-faint . "#a08042")
          (blue . "#275ccf")
          (blue-alt . "#475cc0")
          (blue-alt-other . "#3340ef")
          (blue-faint . "#476ce0")
          (blue-alt-faint . "#575ccf")
          (blue-alt-other-faint . "#3f60d7")
          (magenta . "#bf317f")
          (magenta-alt . "#d033c0")
          (magenta-alt-other . "#844fe4")
          (magenta-faint . "#bf517f")
          (magenta-alt-faint . "#d053c0")
          (magenta-alt-other-faint . "#846fe4")
          (cyan . "#007a9f")
          (cyan-alt . "#3f709f")
          (cyan-alt-other . "#107f7f")
          (cyan-faint . "#108aaf")
          (cyan-alt-faint . "#3f80af")
          (cyan-alt-other-faint . "#3088af")
          (red-active . "#cd2f44")
          (green-active . "#116a6c")
          (yellow-active . "#993602")
          (blue-active . "#475ccf")
          (magenta-active . "#7f2ccf")
          (cyan-active . "#007a8f")
          (red-nuanced-bg . "#ffdbd0")
          (red-nuanced-fg . "#ed6f74")
          (green-nuanced-bg . "#dcf0dd")
          (green-nuanced-fg . "#3f9a4c")
          (yellow-nuanced-bg . "#fff3aa")
          (yellow-nuanced-fg . "#b47232")
          (blue-nuanced-bg . "#e3e3ff")
          (blue-nuanced-fg . "#201f6f")
          (magenta-nuanced-bg . "#fdd0ff")
          (magenta-nuanced-fg . "#c0527f")
          (cyan-nuanced-bg . "#dbefff")
          (cyan-nuanced-fg . "#0f3f60")
          (bg-diff-heading . "#b7cfe0")
          (fg-diff-heading . "#041645")
          (bg-diff-added . "#d6f0d6")
          (fg-diff-added . "#004520")
          (bg-diff-changed . "#fcefcf")
          (fg-diff-changed . "#524200")
          (bg-diff-removed . "#ffe0ef")
          (fg-diff-removed . "#891626")
          (bg-diff-refine-added . "#84cfa4")
          (fg-diff-refine-added . "#002a00")
          (bg-diff-refine-changed . "#cccf8f")
          (fg-diff-refine-changed . "#302010")
          (bg-diff-refine-removed . "#da92b0")
          (fg-diff-refine-removed . "#500010")
          (bg-diff-focus-added . "#a6e5c6")
          (fg-diff-focus-added . "#002c00")
          (bg-diff-focus-changed . "#ecdfbf")
          (fg-diff-focus-changed . "#392900")
          (bg-diff-focus-removed . "#efbbcf")
          (fg-diff-focus-removed . "#5a0010"))
        modus-themes-vivendi-color-overrides
        '((bg-main . "#25152a")
          (bg-dim . "#2a1930")
          (bg-alt . "#382443")
          (bg-hl-line . "#332650")
          (bg-active . "#463358")
          (bg-inactive . "#2d1f3a")
          (bg-active-accent . "#50308f")
          (bg-region . "#5d4a67")
          (bg-region-accent . "#60509f")
          (bg-region-accent-subtle . "#3f285f")
          (bg-header . "#3a2543")
          (bg-tab-active . "#26162f")
          (bg-tab-inactive . "#362647")
          (bg-tab-inactive-accent . "#36265a")
          (bg-tab-inactive-alt . "#3e2f5a")
          (bg-tab-inactive-alt-accent . "#3e2f6f")
          (fg-main . "#debfe0")
          (fg-dim . "#d0b0da")
          (fg-alt . "#ae85af")
          (fg-unfocused . "#8e7f9f")
          (fg-active . "#cfbfef")
          (fg-inactive . "#b0a0c0")
          (fg-docstring . "#c8d9f7")
          (fg-comment-yellow . "#cf9a70")
          (fg-escape-char-construct . "#ff75aa")
          (fg-escape-char-backslash . "#dbab40")
          (bg-special-cold . "#2a3f58")
          (bg-special-faint-cold . "#1e283f")
          (bg-special-mild . "#0f3f31")
          (bg-special-faint-mild . "#0f281f")
          (bg-special-warm . "#44331f")
          (bg-special-faint-warm . "#372213")
          (bg-special-calm . "#4a314f")
          (bg-special-faint-calm . "#3a223f")
          (fg-special-cold . "#c0b0ff")
          (fg-special-mild . "#bfe0cf")
          (fg-special-warm . "#edc0a6")
          (fg-special-calm . "#ff9fdf")
          (bg-completion . "#502d70")
          (bg-completion-subtle . "#451d65")
          (red . "#ff5f6f")
          (red-alt . "#ff8f6d")
          (red-alt-other . "#ff6f9d")
          (red-faint . "#ffa0a0")
          (red-alt-faint . "#f5aa80")
          (red-alt-other-faint . "#ff9fbf")
          (green . "#51ca5c")
          (green-alt . "#71ca3c")
          (green-alt-other . "#51ca9c")
          (green-faint . "#78bf78")
          (green-alt-faint . "#99b56f")
          (green-alt-other-faint . "#88bf99")
          (yellow . "#f0b262")
          (yellow-alt . "#f0e242")
          (yellow-alt-other . "#d0a272")
          (yellow-faint . "#d2b580")
          (yellow-alt-faint . "#cabf77")
          (yellow-alt-other-faint . "#d0ba95")
          (blue . "#778cff")
          (blue-alt . "#8f90ff")
          (blue-alt-other . "#8380ff")
          (blue-faint . "#82b0ec")
          (blue-alt-faint . "#a0acef")
          (blue-alt-other-faint . "#80b2f0")
          (magenta . "#ff70cf")
          (magenta-alt . "#ff77f0")
          (magenta-alt-other . "#ca7fff")
          (magenta-faint . "#e0b2d6")
          (magenta-alt-faint . "#ef9fe4")
          (magenta-alt-other-faint . "#cfa6ff")
          (cyan . "#30cacf")
          (cyan-alt . "#60caff")
          (cyan-alt-other . "#40b79f")
          (cyan-faint . "#90c4ed")
          (cyan-alt-faint . "#a0bfdf")
          (cyan-alt-other-faint . "#a4d0bb")
          (red-active . "#ff6059")
          (green-active . "#64dc64")
          (yellow-active . "#ffac80")
          (blue-active . "#4fafff")
          (magenta-active . "#cf88ff")
          (cyan-active . "#50d3d0")
          (red-nuanced-bg . "#440a1f")
          (red-nuanced-fg . "#ffcccc")
          (green-nuanced-bg . "#002904")
          (green-nuanced-fg . "#b8e2b8")
          (yellow-nuanced-bg . "#422000")
          (yellow-nuanced-fg . "#dfdfb0")
          (blue-nuanced-bg . "#1f1f5f")
          (blue-nuanced-fg . "#bfd9ff")
          (magenta-nuanced-bg . "#431641")
          (magenta-nuanced-fg . "#e5cfef")
          (cyan-nuanced-bg . "#042f49")
          (cyan-nuanced-fg . "#a8e5e5")
          (bg-diff-heading . "#304466")
          (fg-diff-heading . "#dae7ff")
          (bg-diff-added . "#0a383a")
          (fg-diff-added . "#94ba94")
          (bg-diff-changed . "#2a2000")
          (fg-diff-changed . "#b0ba9f")
          (bg-diff-removed . "#50163f")
          (fg-diff-removed . "#c6adaa")
          (bg-diff-refine-added . "#006a46")
          (fg-diff-refine-added . "#e0f6e0")
          (bg-diff-refine-changed . "#585800")
          (fg-diff-refine-changed . "#ffffcc")
          (bg-diff-refine-removed . "#952838")
          (fg-diff-refine-removed . "#ffd9eb")
          (bg-diff-focus-added . "#1d4c3f")
          (fg-diff-focus-added . "#b4dfb4")
          (bg-diff-focus-changed . "#424200")
          (fg-diff-focus-changed . "#d0daaf")
          (bg-diff-focus-removed . "#6f0f39")
          (fg-diff-focus-removed . "#eebdba")))
#+end_src
* 🌞 Org-mode                                                      :组织模式:
** ⌚️ Agenda
#+begin_src emacs-lisp
  (with-eval-after-load  "org"
    (setq org-agenda-files (list "~/Repos/logseq/pages/2021-agenda.org"
                                 "~/Repos/logseq/pages/2022-agenda.org"
                                 "~/Repos/logseq/journals")))

  (setq org-todo-keywords
        '((sequence "TODO" "LATER" "NOW" "|" "DONE" "CANCEL")))

  (defun org-agenda-show-all-todo (&optional arg)
    "list all my todo"
    (interactive "P")
    (org-agenda arg "t"))
#+end_src
** 🖋️ Latex
#+begin_src emacs-lisp
  (with-eval-after-load  "org"
    (setq org-format-latex-options (plist-put org-format-latex-options :scale 4.0))
    (setq org-preview-latex-default-process 'dvisvgm))

#+end_src
** 👚 Style
#+begin_src emacs-lisp
  (setq org-image-actual-width 500)
  ;; org-mode 自动对齐
  (setq org-startup-indented t)
  ;; org-mode 自动折叠
  (setq org-startup-folded 'content)

  ;; (add-hook 'org-mode-hook #'org-modern-mode)
#+end_src
** 🧾 HTML
#+begin_src emacs-lisp
  (sup 'htmlize)
  (setq org-export-with-toc t)
#+end_src
** 💫 Modern
#+begin_src emacs-lisp
  (sup 'org-modern)

  (add-hook 'org-mode-hook #'org-modern-mode)
  (add-hook 'org-agenda-finalize-hook #'org-modern-agenda)
#+end_src
* 👼🏼 Minibuff                                                :选用原生风格:
#+begin_src emacs-lisp
  ;; Completion
  (if (fboundp 'fido-mode)
      (progn
        (fido-mode 1)
        (when (fboundp 'fido-vertical-mode)
          (fido-vertical-mode 1))

        (defun fido-recentf-open ()
          "Use `completing-read' to find a recent file."
          (interactive)
          (if (find-file (completing-read "Find recent file: " recentf-list))
              (message "Opening file...")
            (message "Aborting")))
        (global-set-key (kbd "C-c r") 'fido-recentf-open)))
#+end_src
* ⌨️ Keybind                                                        :键位设定:
#+begin_src emacs-lisp
  (global-set-key (kbd "C-c p p") 'project-switch-project)
  (global-set-key (kbd "C-c p f") 'project-find-file)
  (global-set-key (kbd "C-c p c") 'project-compile)
  (global-set-key (kbd "C-c p s") 'project-search)
  (global-set-key (kbd "C-c v v") 'vc-next-action)
  (global-set-key (kbd "C-c v c") 'vc-dir)
  (global-set-key (kbd "C-c v d") 'vc-diff)
  (global-set-key (kbd "C-c w w") 'windmove-up)
  (global-set-key (kbd "C-c w a") 'windmove-left)
  (global-set-key (kbd "C-c w s") 'windmove-down)
  (global-set-key (kbd "C-c w d") 'windmove-right)
  (global-set-key (kbd "C-c w q") 'delete-window)
  (global-set-key (kbd "C-c w 1") 'delete-other-windows)
  (global-set-key (kbd "C-c w 2") 'split-window-below)
  (global-set-key (kbd "C-c w 3") 'split-window-right)
  (global-set-key (kbd "C-c b b") 'ido-switch-buffer)
  (global-set-key (kbd "C-c b k") 'ido-kill-buffer)
  (global-set-key (kbd "C-c t t") 'org-agenda-show-all-todo)
#+end_src

* 🐈 Meow                                                      :喵喵喵喵喵喵:
#+begin_src emacs-lisp
  (sup 'meow)

  (prog1 'meow
    (defun meow-setup nil
      (setq meow-cheatsheet-layout meow-cheatsheet-layout-qwerty)
      (meow-motion-overwrite-define-key
       '("j" . meow-next)
       '("k" . meow-prev)
       '("<escape>" . ignore))
      (meow-leader-define-key
       '("j" . "H-j")
       '("k" . "H-k")
       '("1" . meow-digit-argument)
       '("2" . meow-digit-argument)
       '("3" . meow-digit-argument)
       '("4" . meow-digit-argument)
       '("5" . meow-digit-argument)
       '("6" . meow-digit-argument)
       '("7" . meow-digit-argument)
       '("8" . meow-digit-argument)
       '("9" . meow-digit-argument)
       '("0" . meow-digit-argument)
       '("/" . meow-keypad-describe-key)
       '("?" . meow-cheatsheet))
      (meow-normal-define-key
       '("0" . meow-expand-0)
       '("9" . meow-expand-9)
       '("8" . meow-expand-8)
       '("7" . meow-expand-7)
       '("6" . meow-expand-6)
       '("5" . meow-expand-5)
       '("4" . meow-expand-4)
       '("3" . meow-expand-3)
       '("2" . meow-expand-2)
       '("1" . meow-expand-1)
       '("-" . negative-argument)
       '(";" . meow-reverse)
       '("," . meow-inner-of-thing)
       '("." . meow-bounds-of-thing)
       '("[" . meow-beginning-of-thing)
       '("]" . meow-end-of-thing)
       '("a" . meow-append)
       '("A" . meow-open-below)
       '("b" . meow-back-word)
       '("B" . meow-back-symbol)
       '("c" . meow-change)
       '("d" . meow-delete)
       '("D" . meow-backward-delete)
       '("e" . meow-next-word)
       '("E" . meow-next-symbol)
       '("f" . meow-find)
       '("g" . meow-cancel-selection)
       '("G" . meow-grab)
       '("h" . meow-left)
       '("H" . meow-left-expand)
       '("i" . meow-insert)
       '("I" . meow-open-above)
       '("j" . meow-next)
       '("J" . meow-next-expand)
       '("k" . meow-prev)
       '("K" . meow-prev-expand)
       '("l" . meow-right)
       '("L" . meow-right-expand)
       '("m" . meow-join)
       '("n" . meow-search)
       '("o" . meow-block)
       '("O" . meow-to-block)
       '("p" . meow-yank)
       '("q" . meow-quit)
       '("Q" . meow-goto-line)
       '("r" . meow-replace)
       '("R" . meow-swap-grab)
       '("s" . meow-kill)
       '("t" . meow-till)
       '("u" . meow-undo)
       '("U" . meow-undo-in-selection)
       '("v" . meow-visit)
       '("w" . meow-mark-word)
       '("W" . meow-mark-symbol)
       '("x" . meow-line)
       '("X" . meow-goto-line)
       '("y" . meow-save)
       '("Y" . meow-sync-grab)
       '("z" . meow-pop-selection)
       '("'" . repeat)
       '("<escape>" . ignore)))
    (unless window-system
      (defun tempuseescape nil "xxx"
             (interactive)
             (meow-motion-overwrite-define-key
              '("<escape>" . save-buffers-kill-emacs))))
    (require 'meow)
    ;; (meow-setup-indicator)
    (meow-global-mode 1)
    ;; (setq meow-replace-state-name-list
    ;; 	'((normal . "Δ")
    ;; 	  (beacon . "⁂")
    ;; 	  (insert . "_")
    ;; 	  (motion . "M")
    ;; 	  (keypad . "δ")))
    (setq meow-use-clipboard t)
    (meow-setup))
#+end_src

* 👩🏽‍💻 Corfu & Cape                                          :代码补全框架:
** 1️⃣ CAPE
#+begin_src emacs-lisp
  (sup 'cape)

  (prog1 'cape
    (eval-after-load 'corfu
      '(progn
         (add-to-list 'completion-at-point-functions #'cape-file)
         (add-to-list 'completion-at-point-functions #'cape-tex)
         (add-to-list 'completion-at-point-functions #'cape-dabbrev)
         (add-to-list 'completion-at-point-functions #'cape-keyword))))
#+end_src
** 2️⃣ CORFU
#+begin_src emacs-lisp
  (sup 'corfu)

  (prog1 'corfu
    (setq corfu-auto t)
    (global-corfu-mode))
#+end_src
** 3️⃣ Kind-Icon
#+begin_src emacs-lisp
  (sup' kind-icon)
  
  (prog1 'kind-icon
    (eval-after-load 'corfu
      '(progn
         (customize-set-variable 'kind-icon-default-face nil "Customized with leaf in `kind-icon' block")
         (require 'kind-icon)
         (add-to-list 'corfu-margin-formatters #'kind-icon-margin-formatter))))
#+end_src
** 4️⃣ Corfu-Doc
#+begin_src emacs-lisp
  ;; (sup 'corfu-doc)
  ;; (add-hook 'corfu-mode-hook #'corfu-doc-mode)
#+end_src
** 5️⃣ Orderless
#+begin_src emacs-lisp
  (sup  'orderless)

  (setq completion-styles '(orderless)
        completion-category-defaults 'nil
        completion-category-overrides '((file (styles basic partial-completion))))
#+end_src
** 6️⃣ Corfu-Terminal(For TUI)
#+begin_src emacs-lisp
  ;; (sup
  ;;  '(popon :type git :repo "https://codeberg.org/akib/emacs-popon.git"))

  ;; (sup
  ;;  '(corfu-terminal :type git
  ;;                :repo "https://codeberg.org/akib/emacs-corfu-terminal.git"))

  ;; (unless (display-graphic-p)
  ;;   (corfu-terminal-mode +1))
#+end_src
* 🌍 Language                                                  :开始写代码啦:
** 🚀 Eglot
** ❄️ Nix
#+begin_src emacs-lisp
  (sup 'nix-mode)
#+end_src
** 🥎 Clojure
#+begin_src emacs-lisp
  ;; (sup 'cider)
  ;; (add-hook 'clojure-mode-hook 'eglot-ensure)
#+end_src

** 🦀 Rust
#+begin_src emacs-lisp
  ;; (sup 'rust-mode)
  ;; (add-hook 'rust-mode-hook 'eglot-ensure)  
  ;; (when (featurep 'ns)
  ;;   (setenv "LIBRARY_PATH" "/Users/me/.local/share/libiconv"))
#+end_src

** 🦟 Latex
#+begin_src emacs-lisp
  (sup 'auctex)

  (add-hook 'LaTeX-mode-hook
            #'(lambda ()
                (setq-default TeX-engine 'xetex)))
#+end_src

** 🐫 Ocaml
#+begin_src emacs-lisp
  (sup 'tuareg)
  (add-hook 'tuareg-mode-hook
            (lambda() (setq tuareg-mode-name "🐫")))
#+end_src

** 🌲 Tree-Sitter
#+begin_src bash
  git clone  https://github.com/casouri/tree-sitter-module ~/Repos/tree-sitter-module
#+end_src
#+begin_src emacs-lisp
  (setq treesit-extra-load-path '("~/Repos/tree-sitter-module/dist/"))
#+end_src
** 🧙🏻 Haskell
#+begin_src emacs-lisp
  (sup 'haskell-mode)
#+end_src
